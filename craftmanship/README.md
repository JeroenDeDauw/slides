Presentations about [software craftmanship](http://manifesto.softwarecraftsmanship.org).

For an overview of all presentations, view the [top level README file](../README.md).

### Clean Functions

How to create truly clean, well designed, functions.

* Read [the abstract](functions/README.md)
* View [the slides](http://bit.ly/clean-functions)
* Read [the blog post](http://www.bn2vs.com/blog/2013/09/08/clean-functions/)

### The S in STUPID

Why static code is generally harmful and should be rarely used.

* Read [the abstract](static/README.md)
* View [the slides](http://bit.ly/static-code)
* Read [the blog post](http://www.bn2vs.com/blog/2013/11/15/presentation-the-s-in-stupid/)