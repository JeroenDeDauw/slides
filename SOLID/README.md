Presentations on the [SOLID principles](https://en.wikipedia.org/wiki/SOLID).

For an overview of all presentations, view the [top level README file](../README.md).

### The Liskov Substitution Principle

Explains a simple principle to avoid common pitfalls in the use of inheritance.

* View [the slides](http://bit.ly/solid-lsp)